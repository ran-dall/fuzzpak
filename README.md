# fuzzpak

Launch a Flatpak from your terminal with a fuzzy search.

## Usage

Instead of doing this:

```bash
$ flatpak run org.inkscape.Inkscape
```

do this:

```bash
$ fuzzpak inkscape
```

This launches Inkscape, provided that you have a Flatpak with a canonical name containing the string "inkscape".

## Bugs

This is a brute force and fuzzy search function.
On the off chance that you have two applications with canonical names containing the same string, you could launch the wrong application.
For instance, say you have both [Inkscape](https://flathub.org/apps/details/org.inkscape.Inkscape) and [Inky](https://flathub.org/apps/details/com.inklestudios.Inky) installed, and you do this:

```bash
$ fuzzpak ink
```

This would launch Inky instead of Inkscape, because the canonical name of Inky starts with `com`, which comes before `org`.

This isn't actually a bug, it's just the price of using pattern matching to launch an application.

